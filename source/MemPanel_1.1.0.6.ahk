; � Drugwash, 2012.10.04-2023.08.22
;================================================================
;	DIRECTIVES
;================================================================
#Persistent
#WinActivateForce
#SingleInstance, force
#NoEnv
SetBatchLines, -1
SetKeyDelay, 0, 25, Play
SetControlDelay, -1
SetWinDelay, -1
ListLines, Off
SetFormat, Integer, D
DetectHiddenWindows, On
CoordMode, Mouse, Relative
#include lib\updates.ahk
updates()
;================================================================
;@Ahk2Exe-Obey bits, = %A_PtrSize% * 8
;@Ahk2Exe-Obey xbit, = "%A_PtrSize%" > 4 ? "x64" : "x86"
;@Ahk2Exe-Obey type, = "%A_IsUnicode%" ? "Unicode" : "ANSI"
;@Ahk2Exe-Obey xtype, = "%A_IsUnicode%" ? "W" : "A"
;@Ahk2Exe-Let version = 1.1.0.6
;@Ahk2Exe-AddResource %A_ScriptDir%\res\skin0.bmp, 100
;@Ahk2Exe-AddResource *14 %A_ScriptDir%\res\MemPanel-white.ico, 159
;@Ahk2Exe-SetFileVersion %U_version%
;@Ahk2Exe-SetProductVersion 1.1.0.0
;@Ahk2Exe-SetProductName MemPanel
;@Ahk2Exe-SetInternalName MemPanel.ahk
;@Ahk2Exe-SetOrigFilename MemPanel.exe
;@Ahk2Exe-SetDescription MemPanel displays RAM`, swap`, CPU usage`, and total system load
;@Ahk2Exe-SetCompanyName Drugwash Hobby Programming
;@Ahk2Exe-SetCopyright Copyright � Drugwash`, August 2023
;@Ahk2Exe-SetLegalTrademarks Released under the terms of the GNU General Public License
;@Ahk2Exe-Set Comments, Compiled in %A_AhkVersion% %U_type% %U_bits%bit.
;@Ahk2Exe-SetMainIcon %A_ScriptDir%\res\MemPanel-white.ico
;@Ahk2Exe-UpdateManifest 0, Drugwash.HobbyProgramming.MemPanel, %U_version%
;================================================================
;	IDENTIFICATION
;================================================================

author=Drugwash
appname=MemPanel
version=1.1.0.6
releaseD=August 22, 2023
releaseT := "public release " (A_IsUnicode ? "Unicode" : "ANSI")
debug=1
;================================================================
;	VARIABLES
;================================================================

iconlocal=%A_ScriptDir%\res\%appname%-white.ico
;skinlocal=%A_ScriptDir%\res\skin.bmp
;hILS := A_IsCompiled ? ILC_List(4, A_ScriptFullPath, 100, 0) : ILC_List(4, skinlocal, "", 0)
skinlocal=%A_ScriptDir%\res\skin0.bmp
hILS := A_IsCompiled ? ILC_List(2, A_ScriptFullPath, 100, 0) : ILC_List(2, skinlocal, "", 0)
vars := "totalMB,availMB,totalPMB,availPMB,totalVMB,availVMB,load,swap,cpu"
progs := "Pmem,Ppage,Pvirt,Pload,Pswap"
tmropt := ".5|1|1.5|2|2.5|3|4|5|10|20"
sh=1
;================================================================
;	PREFERENCES
;================================================================

IniRead, pos, %appname%_preferences.ini, Settings, Position, % "x" A_ScreenWidth-200 " y33"
IniRead, skin, %appname%_preferences.ini, Settings, Skin, 8
IniRead, poll, %appname%_preferences.ini, Settings, Timer, 4
if poll not between 1 and 10
	poll=4
;================================================================
;	TRAY  MENU
;================================================================

Menu, Tray, UseErrorLevel
if !A_IsCompiled
	Menu, Tray, Icon, %iconlocal%
Menu, Tray, Tip, %appname%
Menu, Tray, NoStandard
Menu, Tray, Add, Hide panel, GuiContextMenu
Menu, Tray, Default, Hide panel
Menu, Tray, Add
Menu, Tray, Add, Options, optshow
Menu, Tray, Add, About, about
Menu, Tray, Add
if debug
	Menu, Tray, Add, Reload, reload
Menu, Tray, Add, Exit, GuiClose
;================================================================
;	MAIN  GUI
;================================================================

Gui, Margin, 0, 0
Gui, Color, White, White
Gui,  +ToolWindow -Caption +Border +AlwaysOnTop
Gui, Font, s7 w400 cBlack, Tahoma
Gui, Add, Progress, x45 y3 w50 h10 Range0-100 BackgroundWhite C800080 vPmem, 100
Gui, Add, Progress, x45 y+0 w50 h10 Range0-100 BackgroundWhite C800080 vPpage, 100
;Gui, Add, Progress, x45 y+0 w50 h10 Range0-100 BackgroundWhite C800080 vPvirt, 100
;Gui, Add, Progress, x45 y+0 w50 h10 Range0-100 BackgroundWhite C800080 vPswap, 100
Gui, Add, Progress, x45 y+0 w50 h10 Range0-100 BackgroundWhite C800080 vPload, 100
Gui, Add, Picture, x0 y0 w140 h36 +0x400000E hwndhSkin gmoveit,
Gui, Add, Text, x3 y3 w40 h10 BackgroundTrans Right vavailMB, %availMB%
Gui, Add, Text, x3 y+0 w40 h10 BackgroundTrans Right vavailPMB, %availPMB%
;Gui, Add, Text, x3 y+0 w40 h10 BackgroundTrans Right vavailVMB, %availVMB%
;Gui, Add, Text, x3 y+0 w40 h10 BackgroundTrans Right vswap, %swap%
Gui, Add, Text, x3 y+0 w40 h10 BackgroundTrans Right vload, %load%
Gui, Add, Text, x97 y3 w40 h10 BackgroundTrans Right vtotalMB, %totalMB%
Gui, Add, Text, x97 y+0 w40 h10 BackgroundTrans Right vtotalPMB, %totalPMB%
;Gui, Add, Text, x97 y+0 w40 h10 BackgroundTrans Right vtotalVMB, %totalVMB%
Gui, Font, w700, Tahoma
;Gui, Add, Text, x97 y+0 w40 h10 BackgroundTrans Center vwarnSwap, %warnSwap%
;Gui, Add, Text, x97 y+0 w40 h10 BackgroundTrans Center vwarnLoad, %warn%
Gui, Font, s7 w800, Tahoma
Gui, Add, Text, x97 y+0 w40 h10 BackgroundTrans Right vcpu, %cpuload%
; Generated using SmartGuiXP Creator mod 4.3.29.1
hbmp := ILC_FitBmp2(hSkin, hILS, skin)
Gui, Show, %pos% AutoSize, %appname%
WinGet, hMain, ID, %appname%
;================================================================
;	SETTINGS  GUI
;================================================================

Gui, 2:Color, White, White
Gui, 2:Margin, 5, 5
Gui, 2:Font, s7, Wingdings
;Gui, 2:Add, Text, x5 y0 w8 h9 0x6 Border BackgroundTrans,
Gui, 2:Add, Text, x5 y0 w8 h10 0x200 Center BackgroundTrans, �
Gui, 2:Font
Gui, 2:Add, Picture, x5 y10 w256 h20 0xE hwndhSelect gselect,
Gui, 2:Add, Text, x5 y+5 w80 h21 0x200 Right, Polling interval :
Gui, 2:Add, DDL, x+1 yp w45 hp R10 AltSubmit Choose%poll% vpoll gsettimer, %tmropt%
Gui, 2:Add, Text, x+1 yp w60 hp 0x200, seconds
Gui, 2:Add, Button, x220 yp w41 hp, OK
Gui, 2:Show, Hide, %appname% options
WinGet, hSet, ID, %appname% options
;================================================================
if A_IsCompiled
	{
	hInst := DllCall("GetModuleHandle", Ptr, NULL)
	hbmp2 := DllCall("LoadImage"
				, Ptr, hInst
				, "UInt", 100
				, "UInt", 0
				, "Int", 256
				, "Int", 20
				, "UInt", 0x2000
				, Ptr)
	}
else hbmp2 := DllCall("LoadImage"
				, Ptr, NULL
				, "Str", skinlocal
				, "UInt", 0
				, "Int", 256
				, "Int", 20
				, "UInt", 0x2010
				, Ptr)
if hP := DllCall("SendMessage" AW
			, Ptr, hSelect
			, "UInt", 0x172	; STM_SETIMAGE
			, "UInt", 0		; IMAGE_BITMAP
			, Ptr, hbmp2
			, Ptr)
	DllCall("DeleteObject", Ptr, hP)
hCursM := DllCall("LoadCursor", Ptr, NULL, "Int", 32646, Ptr)	; IDC_SIZEALL
hCursH := DllCall("LoadCursor", Ptr, NULL, "Int", 32649, Ptr)	; IDC_HAND
gosub settimer
OnExit, save
OnMessage(0x200, "msmove")	; WM_MOUSEMOVE
if A_OSVersion in WIN_NT4,WIN_95,WIN_98,WIN_ME
	{
	VarSetCapacity(ms, 32, 0)		; MEMORYSTATUS struct
	lbl := "GMS"
	}
else
	{
	VarSetCapacity(ms, 64, 0)		; MEMORYSTATUSEX struct
	NumPut(64, ms, 0, "UInt")		; dwLength
	lbl := "GMSEx"
	}
gosub %lbl%
return
;================================================================
GMS:
SetFormat, Float, 0.0
cpuload := cpuchk()
DllCall("GlobalMemoryStatus", Ptr, &ms)
load := NumGet(ms, 4, "UInt")		; dwMemoryLoad
total := NumGet(ms, 8, "UInt")		; dwTotalPhys
avail := NumGet(ms, 12, "UInt")		; dwAvailPhys
totalP := NumGet(ms, 16, "UInt")		; dwTotalPageFile
availP := NumGet(ms, 20, "UInt")		; dwAvailPageFile
totalV := NumGet(ms, 24, "UInt")		; dwTotalVirtual
availV := NumGet(ms, 28, "UInt")		; dwAvailVirtual
gosub common
SetTimer, GMS, % -1000*timer
return
;================================================================
GMSEx:
if !DllCall("GlobalMemoryStatusEx", Ptr, &ms)
	return
cpuload := cpuchkW()
load := NumGet(ms, 4, "UInt")		; dwMemoryLoad
total := NumGet(ms, 8, "UInt64")		; ullTotalPhys
avail := NumGet(ms, 16, "UInt64")		; ullAvailPhys
totalP := NumGet(ms, 24, "UInt64")	; ullTotalPageFile
availP := NumGet(ms, 32, "UInt64")	; ullAvailPageFile
totalV := NumGet(ms, 40, "UInt64")	; ullTotalVirtual
availV := NumGet(ms, 48, "UInt64")	; ullAvailVirtual
availX := NumGet(ms, 56, "UInt64")	; ullAvailExtendedVirtual
gosub common
SetTimer, GMSEx, % -1000*timer
return

common:
totalMB := Ceil(total/(1024*1024))
availMB := avail/(1024*1024)
totalPMB := totalP/(1024*1024)
availPMB := availP/(1024*1024)
totalVMB := totalV/(1024*1024)
availVMB := availV/(1024*1024)
availXMB := availX/(1024*1024)
swap := totalPMB-availPMB
Pmem := (avail*100)//total
Ppage := (availPMB*100)//totalPMB
Pvirt := (availVMB*100)//totalVMB
Pswap := (swap*100)//totalPMB
Pload := Round(load)
Loop, Parse, vars, CSV
	GuiControl,, %A_LoopField%
		, % (A_LoopField="load" ? %A_LoopField% "`%" : %A_LoopField% " M")
Loop, Parse, progs, CSV
	GuiControl,, %A_LoopField%, % %A_LoopField%
GuiControl,, warnSwap, % Pswap > 95 ? "SWAP" : ""
;GuiControl,, warnLoad, % Pload > 90 ? "LOAD" : ""
GuiControl,, cpu, %cpuload%`%
return
;================================================================
;================================================================
reload:
Reload
Sleep, 3000
GuiClose:
ExitApp
;================================================================
GuiContextMenu:
sh := !sh
Gui, % (sh ? "Show" : "Hide")
Menu, Tray, Rename, % (sh ? "Show" : "Hide") " panel", % (sh ? "Hide" : "Show") " panel"
return
;================================================================
save:
SetTimer, GMS, Off
SetTimer, GMSEx, Off
cpuchk(TRUE)
WinGetPos, wx, wy,,, %appname%
IniWrite, x%wx% y%wy%, %appname%_preferences.ini, Settings, Position
IniWrite, %skin%, %appname%_preferences.ini, Settings, Skin
IniWrite, %poll%, %appname%_preferences.ini, Settings, Timer
DllCall("DeleteObject", Ptr, hbmp)
DllCall("DeleteObject", Ptr, hbmp2)
;DllCall("DestroyCursor", Ptr, hCursH)
;DllCall("DestroyCursor", Ptr, hCursM)
ExitApp
;================================================================
moveit:
DllCall("SetCursor", Ptr, hCursM)
PostMessage, 0xA1, 2,,, A	; WM_NCLBUTTONDOWN
return
;================================================================
optshow:
Gui, 2:Show
return
;================================================================
select:
MouseGetPos, mx, my, winid, ctrl, 2
ControlGetPos, cX, cY,,, Static2, ahk_id %hSet%
skin := 1+(mx-cX)//8
hbmp := ILC_FitBmp2(hSkin, hILS, skin)
return
;================================================================
settimer:
Gui, 2:Submit, NoHide
Loop, Parse, tmropt, |
	if (poll=A_Index)
		timer := A_LoopField
return
;================================================================
2GuiClose:
2ButtonOK:
Gui, 2:Hide
return
;================================================================
about:
ex := A_IsUnicode ? "Ex" : ""
MsgBox, 0x43040, %appname%,
(
� Drugwash, %releaseD%
version %version% %releaseT%

Monitors RAM, swap, CPU usage.
Display organization as follows:

     Free RAM  [______] Total RAM
     Free swap [______] Total swap
Memory load  [______] CPU load

� Uses GlobalMemoryStatus%ex%() API.
� Not tested on x64 systems.
� Drag with the mouse to position.
� Use tray menu > Options to change skin.
)
return
;================================================================
;	FUNCTIONS
;================================================================
msmove(wP, lP, msg, hwnd)
{
Global Ptr, hSet, hSelect, hCursH
Static hide
MouseGetPos, mx, my, winid, ctrl, 2
if (winid != hSet)
	return
if (ctrl != hSelect)
	{
	if !hide
		{
		GuiControl, 2:Hide, Static1
		hide=TRUE
		}
	return
	}
ControlGetPos, X, Y,,, Static1, ahk_id %hSet%
ControlGetPos, cX, cY,,, Static2, ahk_id %hSet%
s := (mx-cX)//8
if (X != cX+8*s)
	{
	ControlMove, Static1, % cX+8*s,,,, ahk_id %hSet%
	GuiControl, 2:Show, Static1
	hide=
	}
DllCall("SetCursor", Ptr, hCursH)
}
;================================================================
cpuchk(free="")
{
Static
Global Ptr, PtrP, AStr, AW
if !hMod
	{
	hMod := DllCall("LoadLibrary" AW, "Str", "advapi32.dll", Ptr)
	hOpen := DllCall("GetProcAddress", Ptr, hMod, AStr, "RegOpenKeyExA", Ptr)
	hQuery := DllCall("GetProcAddress", Ptr, hMod, AStr, "RegQueryValueExA", Ptr)
	hClose := DllCall("GetProcAddress", Ptr, hMod, AStr, "RegCloseKey", Ptr)
	DllCall(hOpen
		, "UInt", 0x80000006		; hKey (HKEY_DYN_DATA)
		, "Str", "PerfStats\StartStat"	; lpSubKey
		, "UInt", 0				; ulOptions
		, "UInt", 0x1				; samDesired (KEY_QUERY_VALUE)
		, PtrP, hReg)				; phkResult
	DllCall(hQuery
		, Ptr, hReg				; hKey
		, "Str", "KERNEL\CPUUsage"	; lpValueName
		, "UInt", 0				; lpReserved
		, PtrP, r:=4				; lpType
		, PtrP, key				; lpData
		, PtrP, sz:=4)				; lpcbData
	DllCall(hClose, Ptr, hReg)
	DllCall(hOpen
		, "UInt", 0x80000006		; hKey (HKEY_DYN_DATA)
		, "Str", "PerfStats\StatData"	; lpSubKey
		, "UInt", 0				; ulOptions
		, "UInt", 0x1				; samDesired (KEY_QUERY_VALUE)
		, PtrP, hReg)				; phkResult
	}
else if (hMod && free)
	{
	DllCall(hClose, Ptr, hReg)
	DllCall(hOpen
		, "UInt", 0x80000006		; hKey (HKEY_DYN_DATA)
		, "Str", "PerfStats\StopStat"	; lpSubKey
		, "UInt", 0				; ulOptions
		, "UInt", 0x1				; samDesired (KEY_QUERY_VALUE)
		, PtrP, hReg)				; phkResult
	DllCall(hQuery
		, Ptr, hReg				; hKey
		, "Str", "KERNEL\CPUUsage"	; lpValueName
		, "UInt", 0				; lpReserved
		, PtrP, r:=4				; lpType
		, PtrP, key				; lpData
		, PtrP, sz:=4)				; lpcbData
	DllCall(hClose, Ptr, hReg)
	return DllCall("FreeLibrary", Ptr, hMod)
	}
DllCall(hQuery
	, Ptr, hReg					; hKey
	, "Str", "KERNEL\CPUUsage"		; lpValueName
	, "UInt", 0					; lpReserved
	, PtrP, r:=4					; lpType
	, PtrP, key					; lpData
	, PtrP, sz:=4)					; lpcbData
return key
}
;================================================================
cpuchkW()
{
; http://www.autohotkey.com/board/topic/11910-cpu-usage/
Static
Global Ptr
SetFormat, Float, 2
if !cpus
	{
	EnvGet, cpus, NUMBER_OF_PROCESSORS
	cpus := cpus ? cpus : 1
	VarSetCapacity(ft, 8)	; FILETIME struct
	}
oTime := cTime, oTick := cTick
DllCall("GetSystemTimes", Ptr,&ft, "UInt",0, "UInt",0)
cTime := NumGet(ft, 0, "UInt") + (NumGet(ft, 4, "UInt") << 32)
cTick := A_TickCount
load := 100 - 0.01*(cTime - oTime)/cpus/(cTick - oTick)
Return, Abs(load)
}
;================================================================
;	INCLUDES
;================================================================
#include lib\func_ImageList.ahk
